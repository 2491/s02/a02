// alert("B249!");

// create student one
let studentOneName = 'John';
let studentOneEmail = 'john@mail.com';
let studentOneGrades = [89, 84, 78, 88];

// create student one
let studentTwoName = 'Joe';
let studentTwoEmail = 'joe@mail.com';
let studentTwoGrades = [78, 82, 79, 85];

// create student three
let studentThreeName = 'Jane';
let studentThreeEmail = 'jane@mail.com';
let studentThreeGrades = [87, 89, 91, 93];

// create student four
let studentFourName = 'Jessie';
let studentFourEmail = 'jessie@mail.com';
let studentFourGrades = [91, 89, 92, 93];


// actions that students may perform
function login(email) {
	console.log(`${email} has logged in`);
}
login(studentOneEmail);

function logout(email) {
	console.log(`${email} has logged out`);
}

function listGrades(email) {
	grades.forEach(grade => {
		console.log(grade)
	})
}

// spaghetti code - code that is so poorly organized that it becomes impossible to work with

// use an object literal: {}
// ENCAPSULATION - the organization of information (properties) and behaviour (as methods) to belong to the object that encapsulates them (the scope of encapsulation is denoted by the object literals)

let studentOne = {
	name: 'John',
	email: 'john@mail.com',
	grades: [89, 84, 78, 88],

	// add the functionalities available to a student as object methods
		// the keyword "this" refers to the object encapsulating the method where "this" is called.
	login() {
		console.log(`${this.email} has logged in`);
	},
	logout() {
		console.log(`${this.email} has logged out`);
	},
	listGrades() {
		console.log(`${this.name}'s' quarterly grade average s are: ${this.grades}`);
	},
	computeAve() {
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade); 
		return sum/4;
	},
	willPass(){
		return this.computeAve() >= 85 ? true : false
	},
	willPassWithHonors(){
		return (this.willPass() && this.computeAve() >= 90) ? true : false
	}
};

/*
	Mini -Exercise:
		//Create a function that will get/compute the quarterly average of the studentOne's grades:

	Mini - Exercise 2:
		// Create a function that will return true if the average grade is >= 85, false if not 
	willPass(){
		//hint: you can call methods inside an object
	}
*/


console.log(`student one's name is ${studentOne.name}`);
console.log(`student one's name is ${studentOne.email}`);
console.log(`student one's name is ${studentOne.grades}`);


// ACTIVITY - QUIZ
/*
1. spaghetti code
2. Enclose everything in a curly brace {}
3. method
4. boolean 
5. true
6. using dot notation or square bracket notation
7. true
8. true
9. true
10. true

*/

//Function Coding:
//1-4

let studentTwo = {
	name: 'Joe',
	email: 'joe@mail.com',
	grades: [78, 82, 79, 85],

	login() {
		console.log(`${this.email} has logged in`);
	},
	logout() {
		console.log(`${this.email} has logged out`);
	},
	listGrades() {
		console.log(`${this.name}'s' quarterly grade average s are: ${this.grades}`);
	},
	computeAve() {
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade); 
		return sum/4;
	},
	willPass(){
		return this.computeAve() >= 85 ? true : false
	},
	willPassWithHonors(){
		return (this.willPass() && this.computeAve() >= 90) ? true : false;
		if(this.willPass() && this.computeAve() <85) undefined
	}

};


let studentThree = {
	name: 'Jane',
	email: 'jane@mail.com',
	grades: [87, 89, 91, 93],

	login() {
		console.log(`${this.email} has logged in`);
	},
	logout() {
		console.log(`${this.email} has logged out`);
	},
	listGrades() {
		console.log(`${this.name}'s' quarterly grade average s are: ${this.grades}`);
	},
	computeAve() {
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade); 
		return sum/4;
	},
	willPass(){
		return this.computeAve() >= 85 ? true : false
	},
	willPassWithHonors(){
		return (this.willPass() && this.computeAve() >= 90) ? true : false
	}
};

let studentFour = {
	name: 'Jessie',
	email: 'jessie@mail.com',
	grades: [91, 89, 92, 93],

	login() {
		console.log(`${this.email} has logged in`);
	},
	logout() {
		console.log(`${this.email} has logged out`);
	},
	listGrades() {
		console.log(`${this.name}'s' quarterly grade average s are: ${this.grades}`);
	},
	computeAve() {
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade); 
		return sum/4;
	},
	willPass(){
		return this.computeAve() >= 85 ? true : false
	},
	willPassWithHonors(){
		return (this.willPass() && this.computeAve() >= 90) ? true : false;
		if(this.willPass() && this.computeAve() <85) undefined
	}
};

//5.

let classOf1A = {
	students : [studentOne, studentTwo, studentThree, studentFour]
}